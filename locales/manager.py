import yaml

class Locale:
	def __init__(self, file) -> None:
		self.config = None
		self.file = file
	
	def setup(self):
		try:
			with open(self.file, "r", encoding="utf-8") as reader:
				self.config = config = yaml.safe_load(reader)
				print(f"Locale::setup() -> {self.file} loaded.")
				return self
		except FileNotFoundError:
			print("Locale::setup() -> File not found!")
			exit(1)
	
	def get(self):
		return self.config
import requests
from packaging import version

def getCurrentVersion() -> str:
	with open("version", "r", encoding="utf-8") as reader:
		return reader.readline()

def check() -> bool:
	gitVersion = version.parse(requests.get("https://gitlab.com/ThIsIsTails/archpresence/-/raw/main/version").text)

	ver = version.parse(getCurrentVersion())

	return ver < gitVersion

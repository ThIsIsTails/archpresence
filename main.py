import pypresence as pres
from pypresence import DiscordError, DiscordNotFound
from locales.manager import Locale
from datetime import datetime

import os, platform, time

if platform.system() == "Linux": import daemon

import updates

# You can replace it (:
logo = """
                    _     _____                                  
     /\            | |   |  __ \                                 
    /  \   _ __ ___| |__ | |__) _ __ ___ ___  ___ _ __   ___ ___ 
   / /\ \ | '__/ __| '_ \|  ___| '__/ _ / __|/ _ | '_ \ / __/ _ \\
  / ____ \| | | (__| | | | |   | | |  __\__ |  __| | | | (_|  __/
 /_/    \_|_|  \___|_| |_|_|   |_|  \___|___/\___|_| |_|\___\___|                                                            
"""

config = Locale(os.path.join("config.yml")).setup().get()
loc = Locale(os.path.join(f"locales/{config['locale']}.yml")).setup().get()
debug = bool(config["linuxNoDaemon"])

print(logo)
print(str(loc["starting"][0]).format(updates.getCurrentVersion()))
print(loc["starting"][1], end="\n\n")

if platform.system() == "Windows":
    print(loc["warnings"]["windows"][0])
    print(loc["warnings"]["windows"][1])
if platform.system() == "Linux":
    print(loc["warnings"]["linux"][0])

if updates.check() and bool(config["checkForUpdates"]):
    print(loc["update"])

def main():
    
    rpc = pres.Presence(config["client_id"])
    try:
        rpc.connect()
        print(loc["success"])
    except DiscordNotFound:
        print(loc["discord"])
        exit(1)
    except DiscordError as error:
        if error.message == "Invalid Client ID":
            print(loc["app"])
            exit(1)
        
        print("Unexpected error: " + error.message)

    timestamp = datetime.now().timestamp() if bool(config["profile"]["timestamp"]) else None
    details = config["profile"]["details"]
    state = config["profile"]["state"]

    big = config["images"]["big"]["name"]
    bigText = config["images"]["big"]["text"]

    small = config["images"]["small"]["name"]
    smallText = config["images"]["small"]["text"]

    buttons = config["buttons"] if len(list(config["buttons"])) != 0 else None

    while True:
        rpc.update(
            state=state,
            details=details,
            start=int(timestamp),
            large_image=big,
            large_text=bigText,
            small_image=small,
            small_text=smallText,
            buttons=buttons
        )
        time.sleep(15)

if __name__ == "__main__":
    if not debug and platform.system() == "Linux":
        with daemon.DaemonContext():
            main()
    else:
        main()

# ArchPresence

## Installation

In order to install ArchPresence you must have python 3.x version installed.
Next, you need to install the necessary packages for the script to work.

```sh
# Windows
pip install -r req.txt

#Linux
pip3 install -r req.txt
```

After installing the packages, you need to configure the config.
Change the language to your own, as the default is Russian (All languages are in the `locales` folder and you can add your own if you want).
Create and copy the client ID, then paste it into the `client_id` field in the `config.yml` file

```yml
client_id: client_id_here
```

After that, you can run the script itself.

```sh
# Windows
py main.py

# Linux
python3 main.py
```
